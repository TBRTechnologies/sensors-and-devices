#pragma once
class I2C
{
public:
	char deviceAddress = 0;
	int commSpeed = 100; // 100k
	char i2cBuffer[30];
	int file;
	char* filename;
	const char *buffer;

	I2C();
	I2C(char deviceAddress);
	I2C(char deviceAddress, int speed);

	~I2C();

	int init(char deviceAddress);
	void setRegister(char register);
	void writeData(int fileLocation, char startingAddress, char* dataToWrite, int numberOfBytes);
	void readData(char startingAddress, char* dataRead, int numberOfBytes);

};

