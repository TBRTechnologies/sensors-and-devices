#include "I2C.h"

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

I2C::I2C()
{
}

I2C::I2C(char address)
{
	deviceAddress = address;
}

I2C::I2C(char address, int speed)
{
	deviceAddress = address;
	speed = commSpeed;

}

I2C::~I2C()
{
	;
}

int I2C::init(char deviceAddress)
{
	filename = "/dev/i2c-1";
	if ((file = open(filename, O_RDWR)) < 0)
	{
		/* ERROR HANDLING: you can check errno to see what went wrong */
		perror("Failed to open the i2c bus");
		//exit(1);
	}
	if (ioctl(file, I2C_SLAVE, deviceAddress) < 0)
	{
		printf("Failed to acquire bus access and/or talk to slave.\n");
		/* ERROR HANDLING; you can check errno to see what went wrong */
		//exit(1);
	}
	else
	{
		return file;
	}
}

void I2C::setRegister(char registerToSet)
{
	i2cBuffer[0] = registerToSet;
	if (write(file, i2cBuffer, 1) != 1)	// set the starting register
	{
		printf("Failed to set the register address");
	}
}

void I2C::readData(char startingAddress, char* dataRead, int numberOfBytes)
{
	// Using I2C Read
	int count = 0;

	//setRegister(startingAddress);
	dataRead[0] = startingAddress;
	if ((count = read(file, dataRead, numberOfBytes)) != numberOfBytes)
	{
		/* ERROR HANDLING: i2c transaction failed */
		printf("Failed to read from the i2c bus.\n");
		//buffer = g_strerror(errno);
		//printf(buffer);
		//printf("\n\n");
	}
	else 
	{
		//data = (float)((dataRead[0] & 0b00001111) << 8) + dataRead[1];
		//data = data / 4096 * 5;
		//channel = ((buf[0] & 0b00110000) >> 4);
		//printf("Channel %02d Data:  %04f\n", channel, data);
	}

}

void I2C::writeData(int fileLocation, char startingAddress, char* dataToWrite, int numberOfBytes)
{
	if (write(file, dataToWrite, numberOfBytes) != numberOfBytes)
	{
		/* ERROR HANDLING: i2c transaction failed */
		printf("Failed to write to the i2c bus.\n");
	//	buffer = error(errno);
	//	printf(buffer);
	//	printf("\n\n");
	}
}

