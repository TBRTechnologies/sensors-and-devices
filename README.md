# Sensors and Devices

A collection of sensors and devices are in this collection, using a variety of communications methods, including SPI, I2C and others. It is targeted for the Raspberry Pi running Raspbian, but can be easily modified for other distros and os's. It is written on VS2017 and cross compiled to the Pi over ethernet.