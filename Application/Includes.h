#pragma once

#include "I2C.h"
#include "PCA9685.h"
#include "PCF8574A.h"
#include "AdaFruit_DcMotor.h"
#include "AdaFruit_ServoMotor.h"
#include "BME280.h"
#include "HTS221.h"
#include "LPS25HB.h"
#include "LSM9DS1.h"
#include "SenseHat_JoyStick.h"
#include "SenseHat_LEDs.h"
#include <wiringPi.h>

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>

