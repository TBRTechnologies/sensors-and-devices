#include "Includes.h"

// LED Pin - wiringPi pin 0 is BCM_GPIO 17.
// we have to use BCM numbering when initializing with wiringPiSetupSys
// when choosing a different pin number please use the BCM numbering, also
// update the Property Pages - Build Events - Remote Post-Build Event command 
// which uses gpio export for setup for wiringPiSetupSys

#define	LED             17
#define LED1             4
#define LED2            27
#define LED3            22
#define LED4            23
#define INTERRUPT_PIN_0 16
#define INTERRUPT_PIN_1 18 //

void joystickInterrupt(void);

LPS25HB barometer;
SenseHat_JoyStick joystick;
SenseHat_LEDs leds;
AdaFruit_ServoMotor servo;

int main(void)
{
	int x = 1;
	char* filename;
	int file;
	char barometerWho = 0;
	char joystickRead = 0;

	I2C i2cDevice;
	file = i2cDevice.init(0x00);

	barometer.init(file, i2cDevice);
	barometer.readAll();
	x = barometer.getPressure() / 512;
	printf("Barometer: 0x%d\n", x/10);
	x = barometer.whoAmI();
	printf("Barometer: Who Am I 0x%02X\n", x);

	LPS25HB *baro = new LPS25HB();

	joystick.init(file, i2cDevice);
	printf("Joystick: %d", joystick.readJoystick());

	leds.init(file, i2cDevice);

	servo.init(file, i2cDevice);
	servo.setAngle(0, 0);

	filename = "/dev/i2c-1";
	if ((file = open(filename, O_RDWR)) < 0)
	{
		/* ERROR HANDLING: you can check errno to see what went wrong */
		perror("Failed to open the i2c bus\n");
		//exit(1);
	}
	if (ioctl(file, I2C_SLAVE, 0x5c) < 0)
	{
		printf("Failed to acquire bus access and/or talk to slave.\n");
		/* ERROR HANDLING; you can check errno to see what went wrong */
		//exit(1);
	}
	char whoAmI = 0, whoAmI2 = 0;
	int count = 0;
	char dataRead[0x3B];
	char writeBuffer[1] = { 0x00 | 0x80 };
	int data = 0;

	if (write(file, writeBuffer, 1) != 1)	// set the starting register
	{
		printf("Failed to set the register address");
	}

	if ((count = read(file, dataRead, 0x3B)) != 0x3B)
	{
		/* ERROR HANDLING: i2c transaction failed */
		printf("Failed to read from the i2c bus.\n");
		//buffer = g_strerror(errno);
		//printf(buffer);
		//printf("\n\n");
	}
	else
	{
		whoAmI = dataRead[0x0F];
		data = (dataRead[0x2A] << 16) + (dataRead[0x29] << 8) + dataRead[0x28];
		if ((data & (1 << 23)) == 1)
		{
			data = ~data + 1;
		}
		data = data / 4096;
		//channel = ((buf[0] & 0b00110000) >> 4);
		printf("Pressure Data:  %04d\n", data);
	}

	if (ioctl(file, I2C_SLAVE, 0x6a) < 0)
	{
		printf("Failed to acquire bus access and/or talk to slave.\n");
		/* ERROR HANDLING; you can check errno to see what went wrong */
		//exit(1);
	}
	writeBuffer[0] = 0x0F;
	if (write(file, writeBuffer, 1) != 1)	// set the starting register
	{
		printf("Failed to set the register address");
	}

	if ((count = read(file, dataRead, 1)) != 1)
	{
		/* ERROR HANDLING: i2c transaction failed */
		printf("Failed to read from the i2c bus.\n");
		//buffer = g_strerror(errno);
		//printf(buffer);
		//printf("\n\n");
	}
	else
	{
		whoAmI2 = dataRead[0];
		//data = (float)((dataRead[0] & 0b00001111) << 8) + dataRead[1];
		//data = data / 4096 * 5;
		//channel = ((buf[0] & 0b00110000) >> 4);
		//printf("Channel %02d Data:  %04f\n", channel, data);
	}

	// instantiate all chips in system
	//SenseHat_LEDs SenseLeds;
	//HTS221  rhtemp;
	//LSM9DS1 imu;

	wiringPiSetupSys();
	// create function in joystick for interrupt callbak
	pinMode(INTERRUPT_PIN_0, INPUT);
	pinMode(INTERRUPT_PIN_1, INPUT);
	wiringPiISR(INTERRUPT_PIN_1, INT_EDGE_BOTH, &joystickInterrupt);
	pinMode(LED, OUTPUT);
	pinMode(LED1, OUTPUT);
	pinMode(LED2, OUTPUT);

//	barometer.readRegister(barometer.WHO_AM_I);

	while (true)
	{
		servo.setAngle(0, 1);
		servo.setAngle(1, 1);
		digitalWrite(LED, HIGH);  // On
		digitalWrite(LED1, HIGH);  // On
		digitalWrite(LED2, HIGH);  // On
		digitalWrite(LED3, HIGH);  // On
		digitalWrite(LED4, HIGH);  // On
		//printf("Pin Read: %d\n", x);
		delay(1000); // ms
		servo.setAngle(0, 99);
		servo.setAngle(1, 99);
		digitalWrite(LED, LOW);	  // Off
		digitalWrite(LED1, LOW);	  // Off
		digitalWrite(LED2, LOW);	  // Off
		digitalWrite(LED3, LOW);	  // Off
		digitalWrite(LED4, LOW);	  // Off
		delay(1000);

		joystickRead = joystick.readJoystick();
		printf("Joystick Position %d\n", joystickRead);
	}
	return 0;
}

void joystickInterrupt(void)
{
	joystick.ISR_Joystick();
}

