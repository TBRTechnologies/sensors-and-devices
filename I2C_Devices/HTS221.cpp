/*!
******************************************************************************
* @file    HTS221.cpp
* @brief   Sensor Implimentation
* @author  Tom Stokland
* @date    3/29/2018
******************************************************************************/

#include "Includes.h"

HTS221::HTS221()
{
}


HTS221::~HTS221()
{
}

void HTS221::init(int fileNumber, I2C i2cDeviceNumber)
{
	file = fileNumber;
	i2cDevice = i2cDeviceNumber;
	i2cDevice.deviceAddress = deviceAddress;
}

int  HTS221::readRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, int numberOfRegistersToRead)
{
	if (numberOfRegistersToRead > 1)
	{
		registerNumber |= 0x80; // set bit 7 to read sequential registers
	}
	deviceI2c.readData(registerNumber, dataRead, numberOfRegistersToRead);

	return dataRead[0];
}

void HTS221::writeRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, char* dataToWrite, int numberOfRegistersToWrite)
{
	if (numberOfRegistersToWrite > 1)
	{
		registerNumber |= 0x80; // set bit 7 to read sequential registers
	}
	setRegister(i2cFile, device, registerNumber);
	deviceI2c.writeData(file, registerNumber, dataToWrite, numberOfRegistersToWrite);
}

void HTS221::setRegister(int i2cFile, char device, char registerNumber)
{
	if (ioctl(i2cFile, I2C_SLAVE, device) < 0)
	{
		printf("Failed to acquire bus access and/or talk to slave.\n");
		/* ERROR HANDLING; you can check errno to see what went wrong */
		//exit(1);
	}
	dataToWrite[0] = registerNumber;
	if (write(file, dataToWrite, 1) != 1)	// set the starting register
	{
		printf("Failed to set the register address");
	}
}

void HTS221::readAll(void)
{
	readRegister(file, i2cDevice, deviceAddress, FIRST_REGISTER, NUMBER_OF_REGISTERS);
}

char HTS221::whoAmI(void)
{
	return (char)readRegister(file, i2cDevice, deviceAddress, WHO_AM_I, 1);
}

int HTS221::setResolution(bool temperatureResolution, int resolution)
{
	if (resolution > 7)
	{
		return -1;
	}
	char res = getResolution();
	if (temperatureResolution == false)	// do oressure resolutions
	{
		res = (res & 0xF8) | resolution;
	}
	else
	{
		res = (res & 0xC7) | (resolution << 3);
	}
	dataToWrite[0] = resolution;
	writeRegister(file, i2cDevice, deviceAddress, AV_CONF, dataToWrite, 1);
}

int  HTS221::getResolution()
{
	return (char)readRegister(file, i2cDevice, deviceAddress, AV_CONF, 1);
}

int HTS221::setControlRegister(char controlRegister, char registerValue)
{
	if (controlRegister > 3)
	{
		return -1;	// error
	}
	switch (controlRegister)
	{
	case 1:
		dataToWrite[0] = registerValue;
		writeRegister(file, i2cDevice, deviceAddress, CTRL_REG1, dataToWrite, 1);
		break;

	case 2:
		dataToWrite[0] = registerValue;
		writeRegister(file, i2cDevice, deviceAddress, CTRL_REG2, dataToWrite, 1);
		break;

	case 3:
		dataToWrite[0] = registerValue;
		writeRegister(file, i2cDevice, deviceAddress, CTRL_REG3, dataToWrite, 1);
		break;

	default:break;
	}
}

char HTS221::getControlRegister(char controlRegister)
{
	if (controlRegister > 3)
	{
		return -1;	// error
	}
	return (char)readRegister(file, i2cDevice, deviceAddress, (CTRL_REG1 + (controlRegister - 1)), 1);
}

char HTS221::getStatus(void)
{
	return (char)readRegister(file, i2cDevice, deviceAddress, STATUS_REG, 1);
}

int HTS221::getHumidity()
{
	int humidity = 0;
	int rawHumidity = 0;

	readRegister(file, i2cDevice, deviceAddress, HUMIDITY_OUT_L, 2);
	rawHumidity = (dataRead[1] << 8) + dataRead[0];
	humidity = twosComplementToDecimal(rawHumidity, 24);
	return humidity;
}

int HTS221::getTemperature()
{
	int temperature = 0;
	int rawTemperature = 0;

	readRegister(file, i2cDevice, deviceAddress, TEMP_OUT_L, 2);
	rawTemperature = (dataRead[1] << 8) + dataRead[0];
	temperature = twosComplementToDecimal(rawTemperature, 24);
	return temperature;
}

int HTS221::twosComplementToDecimal(int value, int numberOfBits)
{
	if ((value & (1 << numberOfBits)) == 1)
	{
		value = ~value + 1;
	}

	return value;
}

int HTS221::decimalToTwosComplement(int value, int numberOfBits)
{
	int mask = 0xFFFF;

	if (value < 0)
	{
		mask >>= (32 - numberOfBits);
		value = (mask ^ value) - 1;
	}
	return value;
}


