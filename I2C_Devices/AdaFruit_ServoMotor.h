#pragma once

#include "Includes.h"

class AdaFruit_ServoMotor : public PCA9685
{
public:
	char deviceAddress = 0x41;
	int  file = 0;
	I2C  i2cDevice;
	char dataRead[0x3B];
	char dataToWrite[3];
	PCA9685 driver;

	AdaFruit_ServoMotor();
	AdaFruit_ServoMotor(char address);
	~AdaFruit_ServoMotor();

	void init(int fileNumber, I2C i2cDeviceNumber);
	void setAngle(char channel, char angle);

private:
};

