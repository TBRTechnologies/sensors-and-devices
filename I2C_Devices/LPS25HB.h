/*!
******************************************************************************
* @file    LPS25HB.h
* @brief   Sensor Implimentation
* @author  Tom Stokland
* @date    3/29/2018
******************************************************************************/
#pragma once
#include "Includes.h"

class LPS25HB
{
public:
	char deviceAddress = 0x5c;
	int  file = 0;
	I2C  i2cDevice;
	char dataRead[0x3B];
	char dataToWrite[3];

	LPS25HB();
	~LPS25HB();

	enum registers
	{
		FIRST_REGISTER = 0x00,
		REF_P_XL       = 0x08,
		REF_P_L        = 0x09,
		REF_P_H        = 0x0A,
		WHO_AM_I       = 0x0F, // = 0xBD
		RES_CONF       = 0x10,
		CTRL_REG1      = 0x20,
		CTRL_REG2      = 0x21,
		CTRL_REG3      = 0x22,
		CTRL_REG4      = 0x23,
		INTERRUPT_CFG  = 0x24,
		INT_SOURCE     = 0x25,
		STATUS_REG     = 0x27,
		PRESS_OUT_XL   = 0x28,
		PRESS_OUT_L    = 0x29,
		PRESS_OUT_H    = 0x2A,
		TEMP_OUT_L     = 0x2B,
		TEMP_OUT_H     = 0x2C,
		FIFO_CTRL      = 0x2E,
		FIFO_STATUS    = 0x2F,
		THS_P_L        = 0x30,
		THS_P_H        = 0x31,
		RPDS_L         = 0x39,
		RPDS_H         = 0x3A,
		NUMBER_OF_REGISTERS,	// keep as last entry
	};

	void init(int file, I2C i2cDevice);
	int  readRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, int numberOfRegistersToRead);
	void writeRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, char* dataToWrite, int numberOfRegistersToWrite);
	void setRegister(int i2cFile, char device, char registerNumber);

	void readAll(void);
	
	void setReferencePressure(int referencePressure);
	int  getReferencePressure(void);

	char whoAmI(void);

	void setResolution(char resolution);
	char getResolution(void);

	void setControlRegister(char controlRegister, char registerValue);
	char getControlRegister(char controlRegister);

	void setInterruptConfiguration(char interruptConfiguration);
	char getInterruptConfiguration(void);
	char getInterruptSource(void);

	char getStatus(void);

	int  getPressure(void);
	int  getTemperature(void);

	void setFifoControl(char control);
	char getFifoControl(void);
	char getFifoStatus(void);

	void setPressureThreshold(int pressureThreshold);
	int  getPressureThreshold(void);

	void setPressureOffset(int pressureOffset);
	int  getPressureOffset(void);

private:
	int twosComplementToDecimal(int value, int numberOfBits);
	int decimalToTwosComplement(int value, int numberOfBits);
};

