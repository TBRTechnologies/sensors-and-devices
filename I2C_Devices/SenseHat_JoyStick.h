#pragma once
#include "Includes.h"

class SenseHat_JoyStick
{
public:
	const char deviceAddress = 0x46;	// for LED2472G
	int  file = 0;
	I2C  i2cDevice;
	char dataRead[0x3B];
	char dataToWrite[3];
	const int  commandId = 0xF2;
	bool holding = false;

	enum buttons
	{
		NONE  = 0x00,
		DOWN  = 0x01,
		RIGHT = 0x02,
		UP    = 0x04,
		ENTER = 0x08,
		LEFT  = 0x10,
	};

	SenseHat_JoyStick();
	~SenseHat_JoyStick();
	void init(int file, I2C i2cDevice);

	char readJoystick(void);
	bool getHoldStatus(void);
	char ISR_Joystick(void);

private:
	void setRegister(int i2cFile, char device, char registerNumber);
	int  readRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, int numberOfRegistersToRead);
};

