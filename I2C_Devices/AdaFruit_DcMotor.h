#pragma once
#include "Includes.h"

class AdaFruit_DcMotor : public PCA9685
{
public:
	char deviceAddress = 0xA6;
	int  file = 0;
	I2C  i2cDevice;
	char dataRead[0x3B];
	char dataToWrite[3];

	AdaFruit_DcMotor();
	AdaFruit_DcMotor(char address);
	~AdaFruit_DcMotor();

	void init(int fileNumber, I2C i2cDeviceNumber);

private:
	void setRegister(int i2cFile, char device, char registerNumber);
	int  readRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, int numberOfRegistersToRead);
	void writeRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, char* dataToWrite, int numberOfRegistersToWrite);
};

