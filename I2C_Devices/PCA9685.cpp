/*!
******************************************************************************
 * @attention
 * ### COPYRIGHT(c) TBR Technologies (TBR)
 *
 * Created in 2019 as an unpublished copyrighted work. This program and the
 * information contained in it are confidential and proprietary to
 * TBR and may not be used, copied, or reproduced without
 * the prior written permission of TBR
 *
 ******************************************************************************
 * @file      PCA9685.cpp
 * @brief     Interface for the PCA9685 family of PWM drivers (NXP)
 * @author    Tom Stokland
 * @date      4/192019
 * @copyright TBR Technologies
 *
 *
 *
 ******************************************************************************/

#include "PCA9685.h"

PCA9685::PCA9685()
{
}

PCA9685::PCA9685(char address)
{
	deviceAddress = address;
}

PCA9685::~PCA9685()
{
}

void PCA9685::init(int fileNumber, I2C i2cDeviceNumber)
{
	i2cFile = fileNumber;
	i2cDevice = i2cDeviceNumber;
	i2cDevice.deviceAddress = deviceAddress;
	if (ioctl(i2cFile, I2C_SLAVE, deviceAddress) < 0)
	{
		printf("Failed to acquire bus access and/or talk to slave.\n");
		/* ERROR HANDLING; you can check errno to see what went wrong */
		//exit(1);
	}
	//dataToWrite[0] = 0x80;
	//writeRegister(i2cFile, i2cDevice, deviceAddress, MODE_1, dataToWrite, 1);
	//delay(10); // ms
	dataToWrite[0] = MODE_1;
	dataToWrite[1] = 0xA0;
	writeRegister(i2cFile, i2cDevice, deviceAddress, MODE_1, dataToWrite, 2);
	delay(10); // ms
	dataToWrite[0] = MODE_2;
	dataToWrite[1] = 0x04;
	writeRegister(i2cFile, i2cDevice, deviceAddress, MODE_2, dataToWrite, 2);
	//setOutputDrive(false);	// open drain
	//setAutoIncrementMode(true);
	//setPrescaler(50);
}

int PCA9685::readRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, int numberOfRegistersToRead)
{
//	setRegister(i2cFile, device, registerNumber);
	dataRead[0] = registerNumber;
	deviceI2c.readData(registerNumber, dataRead, numberOfRegistersToRead);

	return dataRead[0];
}

//void I2C::writeData(int fileLocation, char startingAddress, char* dataToWrite, int numberOfBytes)
void PCA9685::writeRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, char* dataToWrite, int numberOfRegistersToWrite)
{
	//memcpy(tempDataToWrite, dataToWrite, numberOfRegistersToWrite);
	//setRegister(i2cFile, device, registerNumber);
	//memcpy(dataToWrite, tempDataToWrite, numberOfRegistersToWrite);
	deviceI2c.writeData(i2cFile, registerNumber, dataToWrite, numberOfRegistersToWrite);
}

void PCA9685::setRegister(int i2cFile, char device, char registerNumber)
{
	if (ioctl(i2cFile, I2C_SLAVE, device) < 0)
	{
		printf("Failed to acquire bus access and/or talk to slave.\n");
		/* ERROR HANDLING; you can check errno to see what went wrong */
		//exit(1);
	}
	dataToWrite[0] = registerNumber;
	if (write(i2cFile, dataToWrite, 1) != 1)	// set the starting register
	{
		printf("Failed to set the register address in PCA9685\n");
	}
}

void PCA9685::setExternalClockMode(bool enabled)
{
	int mode = 0;

	mode = readRegister(i2cFile, i2cDevice, deviceAddress, MODE_1, 1);
	enabled ? mode |= EXTCLK : mode &= ~EXTCLK;
	dataToWrite[0] = mode;
	writeRegister(i2cFile, i2cDevice, deviceAddress, MODE_1, dataToWrite, 1);
}

bool PCA9685::getExternalClockMode(void)
{
	int mode = 0;

	mode = readRegister(i2cFile, i2cDevice, deviceAddress, MODE_1, 1);
	return(mode & EXTCLK ? true : false);
}

void PCA9685::setAutoIncrementMode(bool enabled)
{
	int mode = 0;

	mode = readRegister(i2cFile, i2cDevice, deviceAddress, MODE_1, 1);
	enabled ? mode |= AI : mode &= ~AI;
	dataToWrite[0] = mode;
	writeRegister(i2cFile, i2cDevice, deviceAddress, MODE_1, dataToWrite, 1);
}

bool PCA9685::getAutoIncrementMode(void)
{
	int mode = 0;

	mode = readRegister(i2cFile, i2cDevice, deviceAddress, MODE_1, 1);
	return(mode & AI ? true : false);

}

// subaddressNumber is 0 - 2
void PCA9685::enableSubaddress(char subaddressNumber, bool onOff)
{
	int mode = 0;

	mode = readRegister(i2cFile, i2cDevice, deviceAddress, MODE_1, 1);
	onOff ? mode |= SUB1 << subaddressNumber : mode &= ~(SUB1 << subaddressNumber) ;
	dataToWrite[0] = mode;
	writeRegister(i2cFile, i2cDevice, deviceAddress, MODE_1, dataToWrite, 1);
}

void PCA9685::enableAllcallAddress(bool onOff)
{
	int mode = 0;

	mode = readRegister(i2cFile, i2cDevice, deviceAddress, MODE_1, 1);
	onOff ? mode |= ALLCALL : mode &= ~ALLCALL;
	dataToWrite[0] = mode;
	writeRegister(i2cFile, i2cDevice, deviceAddress, MODE_1, dataToWrite, 1);
}

void PCA9685::setSubaddress(char subaddressNumber, char subaddress)
{
	dataToWrite[0] = subaddress;
	writeRegister(i2cFile, i2cDevice, deviceAddress, SUBADDRESS_1 << subaddressNumber, dataToWrite, 1);
}

char PCA9685::getSubaddress(char subaddressNumber)
{
	return readRegister(i2cFile, i2cDevice, deviceAddress, SUBADDRESS_1 << subaddressNumber, 1);
}

void PCA9685::setAllcallAddress(char allcallAddress)
{
	dataToWrite[0] = allcallAddress;
	writeRegister(i2cFile, i2cDevice, deviceAddress, ALL_CALL_ADDRESS, dataToWrite, 1);
}

char PCA9685::getAllcallAddress(void)
{
	return readRegister(i2cFile, i2cDevice, deviceAddress, ALL_CALL_ADDRESS, 1);
}

void PCA9685::setOutputInverted(bool inverted)
{
	int mode = 0;

	mode = readRegister(i2cFile, i2cDevice, deviceAddress, MODE_2, 1);
	inverted ? mode |= ALLCALL : mode &= ~ALLCALL;
	dataToWrite[0] = mode;
	writeRegister(i2cFile, i2cDevice, deviceAddress, MODE_2, dataToWrite, 1);
}

char PCA9685::getOutputInverted(void)
{
	return (readRegister(i2cFile, i2cDevice, deviceAddress, MODE_2, 1) & INVRT);
}

void PCA9685::setOutputChange(bool change)
{
	int mode = 0;

	mode = readRegister(i2cFile, i2cDevice, deviceAddress, MODE_2, 1);
	change ? mode |= OCH : mode &= ~OCH;
	dataToWrite[0] = mode;
	writeRegister(i2cFile, i2cDevice, deviceAddress, INVRT, dataToWrite, 1);
}

char PCA9685::getOutputChange(void)
{
	return (readRegister(i2cFile, i2cDevice, deviceAddress, MODE_2, 1) & OCH);
}

void PCA9685::setOutputDrive(bool drive)
{
	int mode = 0;

	mode = readRegister(i2cFile, i2cDevice, deviceAddress, MODE_2, 1);
	drive ? mode |= OUTDRV : mode &= ~OUTDRV;
	dataToWrite[0] = MODE_2;
	//dataToWrite[1] = mode;
	dataToWrite[1] = 0x0C;
	writeRegister(i2cFile, i2cDevice, deviceAddress, MODE_2, dataToWrite, 2);
}

char PCA9685::getOutputDrive(void)
{
	return (readRegister(i2cFile, i2cDevice, deviceAddress, MODE_2, 1) & OUTDRV);
}

void PCA9685::setOutputNotEnabledMode(char outputMode)
{
	int mode = 0;

	mode = readRegister(i2cFile, i2cDevice, deviceAddress, MODE_2, 1);
	dataToWrite[0] = (mode & 0xFC) | outputMode;
	writeRegister(i2cFile, i2cDevice, deviceAddress, MODE_2, dataToWrite, 1);
}

char PCA9685::getOutputNotEnabledMode(void)
{
	return (readRegister(i2cFile, i2cDevice, deviceAddress, MODE_2, 1) & ~0xFC);
}

void PCA9685::setAngle(char output, char angle)
{
	int onTime = 0;
	int offTime = 0;

	offTime = ((angle * 4095) / 100);
	onTime = ((100 - angle) * 4095) / 100;

	if (onTime == 0)
	{
		onTime = 1;
	}
	if (offTime == 4095)
	{
		offTime = 4094;
	}

	//setOnTime(output, onTime);
	//setOffTime(output, offTime);

	if (ioctl(i2cFile, I2C_SLAVE, deviceAddress) == 0)
	{
		dataToWrite[0] = (LED0_ON_L + (output * 4));
		dataToWrite[1] = onTime & 0xFF;
		writeRegister(i2cFile, i2cDevice, deviceAddress, (LED0_ON_L + (output * 4)), dataToWrite, 2);
		dataToWrite[0] = (LED0_ON_H + (output * 4));
		dataToWrite[1] = (onTime >> 8) & 0xFF;
		writeRegister(i2cFile, i2cDevice, deviceAddress, (LED0_ON_H + (output * 4)), dataToWrite, 2);
		dataToWrite[0] = (LED0_OFF_L + (output * 4));
		dataToWrite[1] = offTime & 0xFF;
		writeRegister(i2cFile, i2cDevice, deviceAddress, (LED0_OFF_L + (output * 4)), dataToWrite, 2);
		dataToWrite[0] = (LED0_OFF_H + (output * 4));
		dataToWrite[1] = (offTime >> 8) & 0xFF;
		writeRegister(i2cFile, i2cDevice, deviceAddress, (LED0_OFF_H + (output * 4)), dataToWrite, 2);
	}
	//dataToWrite[2] = offTime & 0xFF;
	//dataToWrite[3] = (offTime >> 8) & 0xFF;
	//int time = getOffTime(output);
}
void PCA9685::setOnTime(char channel, int onTime)
{
	int time = 0;
	setAutoIncrementMode(true);
//	dataToWrite[1] = (char)(onTime >> 8);
	dataToWrite[0] = 0;
	writeRegister(i2cFile, i2cDevice, deviceAddress, (LED0_ON_L + (4 * channel)), dataToWrite, 1);
	dataToWrite[0] = 0;
	writeRegister(i2cFile, i2cDevice, deviceAddress, (LED0_ON_H + (4 * channel)), dataToWrite, 1);
	time = getOnTime(channel);
	time = getOffTime(channel);

}

int  PCA9685::getOnTime(char channel)
{
	setAutoIncrementMode(true);
	readRegister(i2cFile, i2cDevice, deviceAddress, (LED0_ON_L + (4 * channel)), 2);
	return ((dataRead[1] << 8) + dataRead[0]);
}

void PCA9685::setOffTime(char channel, int offTime)
{
	setAutoIncrementMode(true);
	dataToWrite[1] = (char)(offTime >> 8);
	dataToWrite[0] = (char)(offTime & 0xFF);
	writeRegister(i2cFile, i2cDevice, deviceAddress, (LED0_OFF_L + (4 * channel)), dataToWrite, 2);
}

int  PCA9685::getOffTime(char channel)
{
	int offTime = 0;
//	setAutoIncrementMode(true);
	readRegister(i2cFile, i2cDevice, deviceAddress, (LED0_OFF_L + (4 * channel)), 2);
	offTime = dataRead[0];
	readRegister(i2cFile, i2cDevice, deviceAddress, (LED0_OFF_H + (4 * channel)), 2);
	offTime |= dataRead[0] << 8;
	return ((dataRead[1] << 8) + dataRead[0]);
}

void PCA9685::setPrescaler(char updateRate)
{
	// check SLEEP mode
	// if sleeping, return
	int prescaler = 0;

	prescaler = (25E6 / (4096 * updateRate)) - 1;
	dataToWrite[0] = PRE_SCALE;
	dataToWrite[1] = prescaler;
	writeRegister(i2cFile, i2cDevice, deviceAddress, PRE_SCALE, dataToWrite, 2);
	getPrescaler();
}

char PCA9685::getPrescaler(void)
{
	dataRead[0] = 0;
	readRegister(i2cFile, i2cDevice, deviceAddress, PRE_SCALE, 1);
	return dataRead[0];
}

