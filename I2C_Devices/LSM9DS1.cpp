#include "Includes.h"

LSM9DS1::LSM9DS1()
{
}


LSM9DS1::~LSM9DS1()
{
}

void LSM9DS1::init(int fileNumber, I2C i2cDeviceNumber)
{
	file = fileNumber;
	i2cDevice = i2cDeviceNumber;
	//i2cDevice.deviceAddress = deviceAddress;
}

int LSM9DS1::readRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, int numberOfRegistersToRead)
{
	if (numberOfRegistersToRead > 1)
	{
		registerNumber |= 0x80; // set bit 7 to read sequential registers
	}
	setRegister(i2cFile, device, registerNumber);
	deviceI2c.readData(registerNumber, dataRead, numberOfRegistersToRead);

	return dataRead[0];
}

void LSM9DS1::writeRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, char* dataToWrite, int numberOfRegistersToWrite)
{
	if (numberOfRegistersToWrite > 1)
	{
		registerNumber |= 0x80; // set bit 7 to read sequential registers
	}
	setRegister(i2cFile, device, registerNumber);
	deviceI2c.writeData(file, registerNumber, dataToWrite, numberOfRegistersToWrite);
}

void LSM9DS1::setRegister(int i2cFile, char device, char registerNumber)
{
	if (ioctl(i2cFile, I2C_SLAVE, device) < 0)
	{
		printf("Failed to acquire bus access and/or talk to slave.\n");
		/* ERROR HANDLING; you can check errno to see what went wrong */
		//exit(1);
	}
	dataToWrite[0] = registerNumber;
	if (write(file, dataToWrite, 1) != 1)	// set the starting register
	{
		printf("Failed to set the register address");
	}
}


#pragma region AGRegion
// for sequential register reads/write, set the register address bit 7 passed in

void LSM9DS1::setActivityThreshold(char threshold)
{
	dataToWrite[0] = threshold;
	writeRegister(file, i2cDevice, deviceAddressAccelGyro, ACT_THS, dataToWrite, 1);
}

char LSM9DS1::getActivityThreshold(void)
{
	return readRegister(file, i2cDevice, deviceAddressAccelGyro, ACT_THS, 1);
}

void LSM9DS1::setInactivityDuration(char duration)
{
	dataToWrite[0] = duration;
	writeRegister(file, i2cDevice, deviceAddressAccelGyro, ACT_DUR, dataToWrite, 1);
}

char LSM9DS1::getInactivityDuration(void)
{
	return readRegister(file, i2cDevice, deviceAddressAccelGyro, ACT_DUR, 1);
}

void LSM9DS1::setInterruptGenerationXl(char configuration)
{
	dataToWrite[0] = configuration;
	writeRegister(file, i2cDevice, deviceAddressAccelGyro, INT_GEN_CFG_XL, dataToWrite, 1);
}

char LSM9DS1::getInterruptGenerationXl(void)
{
	return readRegister(file, i2cDevice, deviceAddressAccelGyro, INT_GEN_CFG_XL, 1);
}

void LSM9DS1::setInterruptThreshold(char axis, char threshold)
{
	char registerToWrite = 0;
	switch (axis)
	{
	case 1:
		registerToWrite = INT_GEN_THS_X_XL;
		break;

	case 2:
		registerToWrite = INT_GEN_THS_Y_XL;
		break;

	case 3:
		registerToWrite = INT_GEN_THS_Z_XL;
		break;

	default:break;
	}
	dataToWrite[0] = threshold;
	writeRegister(file, i2cDevice, deviceAddressAccelGyro, registerToWrite, dataToWrite, 1);

}

char LSM9DS1::getInterruptThreshold(char axis)
{
	char registerToWrite = 0;
	switch (axis)
	{
	case 1:
		registerToWrite = INT_GEN_THS_X_XL;
		break;

	case 2:
		registerToWrite = INT_GEN_THS_Y_XL;
		break;

	case 3:
		registerToWrite = INT_GEN_THS_Z_XL;
		break;

	default:break;
	}
	return readRegister(file, i2cDevice, deviceAddressAccelGyro, registerToWrite, 1);
}

void LSM9DS1::setReference(char reference)
{
	dataToWrite[0] = reference;
	writeRegister(file, i2cDevice, deviceAddressAccelGyro, REFERENCE_G, dataToWrite, 1);
}

char LSM9DS1::getReference(void)
{
	return readRegister(file, i2cDevice, deviceAddressAccelGyro, REFERENCE_G, 1);
}

void LSM9DS1::setInterrupt_1_Control(char configuration)
{
	dataToWrite[0] = configuration;
	writeRegister(file, i2cDevice, deviceAddressAccelGyro, INT1_CTRL, dataToWrite, 1);
}

char LSM9DS1::getInterrupt_1_Control(void)
{
	return readRegister(file, i2cDevice, deviceAddressAccelGyro, INT1_CTRL, 1);
}

void LSM9DS1::setInterrupt_2_Control(char configuration)
{
	dataToWrite[0] = configuration;
	writeRegister(file, i2cDevice, deviceAddressAccelGyro, INT2_CTRL, dataToWrite, 1);
}

char LSM9DS1::getInterrupt_2_Control(void)
{
	return readRegister(file, i2cDevice, deviceAddressAccelGyro, INT2_CTRL, 1);
}

char LSM9DS1::whoAmI(void)
{
	return readRegister(file, i2cDevice, deviceAddressAccelGyro, WHO_AM_I, 1);
}

void LSM9DS1::setControlRegisterAG(char registerToWrite, char value)
{
	if (registerToWrite > (CTRL_REG10 - CTRL_REG1_G))
	{
		return;
	}
	dataToWrite[0] = value;
	writeRegister(file, i2cDevice, deviceAddressAccelGyro, (CTRL_REG1_G - 1) + registerToWrite, dataToWrite, 1);
}

char LSM9DS1::getControlRegisterAG(char registerToRead)
{
	if (registerToRead > (CTRL_REG10 - CTRL_REG1_G))
	{
		return 0xFF;
	}
	return readRegister(file, i2cDevice, deviceAddressAccelGyro, (CTRL_REG1_G - 1) + registerToRead, 1);
}

void LSM9DS1::setOrientation(char orientation)
{
	dataToWrite[0] = orientation;
	writeRegister(file, i2cDevice, deviceAddressAccelGyro, ORIENT_CFG_G, dataToWrite, 1);
}

char LSM9DS1::getOrientation(void)
{
	return readRegister(file, i2cDevice, deviceAddressAccelGyro, ORIENT_CFG_G, 1);
}

char LSM9DS1::getInterruptSourceAG(void)
{
	return readRegister(file, i2cDevice, deviceAddressAccelGyro, INT_GEN_SRC_G, 1);
}

char LSM9DS1::getInterruptSourceXl(void)
{
	return readRegister(file, i2cDevice, deviceAddressAccelGyro, INT_GEN_SRC_XL, 1);
}

int  LSM9DS1::getTemperature(void)
{
	int temperature = 0;
	int rawTemperature = 0;

	rawTemperature = readRegister(file, i2cDevice, deviceAddressAccelGyro, OUT_TEMP_L, 2);
	temperature = twosComplementToDecimal(rawTemperature, 16);
	return temperature;
}

int  LSM9DS1::getGyroscope(char axis)	// axis = 0 - 2
{
	int gyroscope = 0;
	int rawGyroscope = 0;

	if (axis > 2)
	{
		return -1;
	}

	rawGyroscope = readRegister(file, i2cDevice, deviceAddressAccelGyro, (OUT_X_L_G + (2 * axis)), 2);
	gyroscope = twosComplementToDecimal(rawGyroscope, 16);
	return gyroscope;
}

char LSM9DS1::getStatusAG(void)
{
	return readRegister(file, i2cDevice, deviceAddressAccelGyro, STATUS_REG, 1);
}

int  LSM9DS1::getAccelerometer(char axis)
{
	int accelerometer = 0;
	int rawAccelerometer = 0;

	if (axis > 2)
	{
		return -1;
	}

	rawAccelerometer = readRegister(file, i2cDevice, deviceAddressAccelGyro, (OUT_X_L_G + (2 * axis)), 2);
	accelerometer = twosComplementToDecimal(rawAccelerometer, 16);
	return accelerometer;
}

void LSM9DS1::setFifoControl(char control)
{
	dataToWrite[0] = control;
	writeRegister(file, i2cDevice, deviceAddressAccelGyro, FIFO_CTRL, dataToWrite, 1);
}

char LSM9DS1::getFifoControl(void)
{
	return readRegister(file, i2cDevice, deviceAddressAccelGyro, FIFO_CTRL, 1);
}

char LSM9DS1::getFifoSource(void)
{
	return readRegister(file, i2cDevice, deviceAddressAccelGyro, FIFO_SRC, 1);
}

void LSM9DS1::setInterruptConfigurationGyro(char configuration)
{
	dataToWrite[0] = configuration;
	writeRegister(file, i2cDevice, deviceAddressAccelGyro, INT_GEN_CFG_G, dataToWrite, 1);
}

char LSM9DS1::getInterruptConfigurationGyro(void)
{
	return readRegister(file, i2cDevice, deviceAddressAccelGyro, INT_GEN_CFG_G, 1);
}

void LSM9DS1::setInterruptGeneratorThresholdGyro(char axis, int threshold)	// axis = 0 - 2
{
	if (axis > 2)
	{
		return;
	}

	dataToWrite[0] = threshold >> 8;
	dataToWrite[1] = threshold & 0xFF;
	writeRegister(file, i2cDevice, deviceAddressAccelGyro, (INT_GEN_THS_XH_G + (2 * axis)), dataToWrite, 1);
}

int  LSM9DS1::getInterruptGeneratorThresholdGyro(char axis)
{
	int threshold = 0;
	int rawThreshold = 0;
	int tempThreshold = 0;

	if (axis > 2)
	{
		return -1;
	}

	tempThreshold = readRegister(file, i2cDevice, deviceAddressAccelGyro, (INT_GEN_THS_XH_G + (2 * axis)), 2);
	rawThreshold = (tempThreshold << 8) + (tempThreshold >> 8);
	threshold = twosComplementToDecimal(rawThreshold, 16);
	return threshold;
}

void LSM9DS1::setInterruptGeneratorDurationGyro(char duration)
{
	dataToWrite[0] = duration;
	writeRegister(file, i2cDevice, deviceAddressAccelGyro, INT_GEN_DUR_G, dataToWrite, 1);
}

char LSM9DS1::getInterruptGeneratorDurationGyro(void)
{
	return readRegister(file, i2cDevice, deviceAddressAccelGyro, INT_GEN_DUR_G, 1);
}

#pragma endregion	// AGRegion

#pragma region Magnetometer

void LSM9DS1::setOffsetM(char axis, int offset) // axis = 0 - 2
{
	if (axis > 2)
	{
		return;
	}

	dataToWrite[0] = offset & 0xFF;
	dataToWrite[1] = offset >> 8;
	writeRegister(file, i2cDevice, deviceAddressMagnetometer, (OFFSET_X_REG_L_M + (2 * axis)), dataToWrite, 2);
}

int  LSM9DS1::getOffsetM(char axis)
{
	int offset = 0;
	int rawOffset = 0;

	if (axis > 2)
	{
		return -1;
	}

	rawOffset = readRegister(file, i2cDevice, deviceAddressMagnetometer, (OFFSET_X_REG_L_M + (2 * axis)), 2);
	offset = twosComplementToDecimal(rawOffset, 16);
	return offset;
}

char LSM9DS1::getWhoAmIM(void)
{
	return readRegister(file, i2cDevice, deviceAddressMagnetometer, WHO_AM_I, 1);
}

void LSM9DS1::setControlRegisterM(char controlRegister, int value)	// controlRegister = 0 - 4
{
	if (controlRegister > 4)
	{
		return;
	}

	dataToWrite[0] = value & 0xFF;
	dataToWrite[1] = value >> 8;
	writeRegister(file, i2cDevice, deviceAddressMagnetometer, (CTRL_REG1_M + (2 * controlRegister)), dataToWrite, 2);
}

char LSM9DS1::getControlRegisterM(char controlRegister)	// controlRegister = 0 - 4
{
	int controlRegisterValue = 0;
	int rawControlRegister = 0;

	if (controlRegister > 4)
	{
		return -1;
	}

	rawControlRegister = readRegister(file, i2cDevice, deviceAddressMagnetometer, (CTRL_REG1_M + (2 * controlRegister)), 2);
	controlRegisterValue = twosComplementToDecimal(rawControlRegister, 16);
	return controlRegisterValue;
}

char LSM9DS1::getStatusRegisterM(void)
{
	return readRegister(file, i2cDevice, deviceAddressMagnetometer, STATUS_REG_M, 1);
}

int  LSM9DS1::getMagnetometer(char axis)
{
	int magnetometer = 0;
	int rawMagnetometer = 0;

	if (axis > 2)
	{
		return -1;
	}

	rawMagnetometer = readRegister(file, i2cDevice, deviceAddressMagnetometer, (OUT_X_L_M + (2 * axis)), 2);
	magnetometer = twosComplementToDecimal(rawMagnetometer, 16);
	return magnetometer;
}

void LSM9DS1::setInterruptConfiguration(char configuration)
{
	dataToWrite[0] = configuration;
	writeRegister(file, i2cDevice, deviceAddressMagnetometer, INT_CFG_M, dataToWrite, 1);
}

char LSM9DS1::getInterruptConfiguration(void)
{
	return readRegister(file, i2cDevice, deviceAddressMagnetometer, INT_CFG_M, 1);
}

char LSM9DS1::getInterruptSourceM(void)
{
	return readRegister(file, i2cDevice, deviceAddressMagnetometer, INT_SRC_M, 1);
}

void LSM9DS1::setInterruptThresholdRegister(int threshold)
{
	dataToWrite[0] = threshold & 0xFF;
	dataToWrite[1] = threshold >> 8;
	writeRegister(file, i2cDevice, deviceAddressMagnetometer, INT_THS_L_M, dataToWrite, 2);
}

int  LSM9DS1::getInterruptThresholdRegister(void)
{
	int threshold = 0;
	int rawThreshold = 0;

	rawThreshold = readRegister(file, i2cDevice, deviceAddressMagnetometer, INT_THS_L_M , 2);
	threshold = twosComplementToDecimal(rawThreshold, 16);
	return threshold;
}

#pragma endregion	// Magnetometer


