#include "Includes.h"

//AdaFruit_ServoMotor::AdaFruit_ServoMotor(char address)
//{
//	deviceAddress = address;
//}
//
AdaFruit_ServoMotor::AdaFruit_ServoMotor()
{
//	deviceAddress = address;
}

AdaFruit_ServoMotor::~AdaFruit_ServoMotor()
{
}

void AdaFruit_ServoMotor::init(int fileNumber, I2C i2cDeviceNumber)
{
	file = fileNumber;
	i2cDevice = i2cDeviceNumber;
	driver.init(fileNumber, i2cDevice);
}

void AdaFruit_ServoMotor::setAngle(char channel, char angle)
{
	driver.setAngle(channel, angle);
}