/*!
******************************************************************************
* @file    BME280.h
* @brief   Sensor Implimentation
* @author  Tom Stokland
* @date    3/29/2018
******************************************************************************/

#pragma once
class BME280
{
public:
	BME280();
	~BME280();

	char deviceAddress = 0x77;
};

