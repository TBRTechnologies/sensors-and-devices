#pragma once
/*!
******************************************************************************
 * @attention
 * ### COPYRIGHT(c) TBR Technologies (TBR)
 *
 * Created in 2019 as an unpublished copyrighted work. This program and the
 * information contained in it are confidential and proprietary to
 * TBR and may not be used, copied, or reproduced without
 * the prior written permission of TBR
 *
 ******************************************************************************
 * @file      PCA9685.h
 * @brief     Interface for the PCA9685 family of PWM drivers (NXP)
 * @author    Tom Stokland
 * @date      4/192019
 * @copyright TBR Technologies
 *
 *
 *
 ******************************************************************************/

//#include "Includes.h"
#include "I2C.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <wiringPi.h>

class PCA9685
{
public:
	char deviceAddress = 0x41; // or 0x41 base = 0x40
	int  i2cFile = 0;
	I2C  i2cDevice;
	char dataRead[0x40];
	char dataToWrite[0x40];
	char tempDataToWrite[0x40];

	enum
	{
		MODE_1 = 0x00,
		MODE_2 = 0x01,
		SUBADDRESS_1 = 0X02,
		SUBADDRESS_2 = 0X03,
		SUBADDRESS_3 = 0X04,
		ALL_CALL_ADDRESS = 0X05,
		LED0_ON_L = 0X06,
		LED0_ON_H = 0X07,
		LED0_OFF_L = 0X08,
		LED0_OFF_H = 0X09,
		LED1_ON_L = 0X0A,
		LED1_ON_H = 0X0B,
		LED1_OFF_L = 0X0C,
		LED1_OFF_H = 0X0D,
		LED2_ON_L = 0X0E,
		LED2_ON_H = 0X0F,
		LED2_OFF_L = 0X10,
		LED2_OFF_H = 0X11,
		LED3_ON_L = 0X12,
		LED3_ON_H = 0X13,
		LED3_OFF_L = 0X14,
		LED3_OFF_H = 0X15,
		LED4_ON_L = 0X16,
		LED4_ON_H = 0X17,
		LED4_OFF_L = 0X18,
		LED4_OFF_H = 0X19,
		LED5_ON_L = 0X1A,
		LED5_ON_H = 0X1B,
		LED5_OFF_L = 0X1C,
		LED5_OFF_H = 0X1D,
		LED6_ON_L = 0X1E,
		LED6_ON_H = 0X1F,
		LED6_OFF_L = 0X20,
		LED6_OFF_H = 0X21,
		LED7_ON_L = 0X22,
		LED7_ON_H = 0X23,
		LED7_OFF_L = 0X24,
		LED7_OFF_H = 0X25,
		LED8_ON_L = 0X26,
		LED8_ON_H = 0X27,
		LED8_OFF_L = 0X28,
		LED8_OFF_H = 0X29,
		LED9_ON_L = 0X2A,
		LED9_ON_H = 0X2B,
		LED9_OFF_L = 0X2C,
		LED9_OFF_H = 0X2D,
		LED10_ON_L = 0X2E,
		LED10_ON_H = 0X2F,
		LED10_OFF_L = 0X30,
		LED10_OFF_H = 0X31,
		LED11_ON_L = 0X32,
		LED11_ON_H = 0X33,
		LED11_OFF_L = 0X34,
		LED11_OFF_H = 0X35,
		LED12_ON_L = 0X36,
		LED12_ON_H = 0X37,
		LED12_OFF_L = 0X38,
		LED12_OFF_H = 0X39,
		LED13_ON_L = 0X3A,
		LED13_ON_H = 0X3B,
		LED13_OFF_L = 0X3C,
		LED13_OFF_H = 0X3D,
		LED14_ON_L = 0X3E,
		LED14_ON_H = 0X3F,
		LED14_OFF_L = 0X40,
		LED14_OFF_H = 0X41,
		LED15_ON_L = 0X42,
		LED15_ON_H = 0X43,
		LED15_OFF_L = 0X44,
		LED15_OFF_H = 0X45,
		// 0X46 - 0XF9 RESERVED
		ALL_LED_ON_L  = 0XFA,
		ALL_LED_ON_H  = 0XFB,
		ALL_LED_OFF_L = 0XFC,
		ALL_LED_OFF_H = 0XFD,
		PRE_SCALE     = 0xFE,

		NUMBER_OF_REGISTERS, // always last
	}PWM_REGISTERS;

	enum
	{
		ALLCALL = 0x01,
		SUB3    = 0x02,
		SUB2    = 0x04,
		SUB1    = 0x08,
		SLEEP   = 0x10,
		AI      = 0x20,
		EXTCLK  = 0x40,
		RESTART = 0x80,
	}MODE1_BITS;

	enum
	{
		OUTNE_0 = 0x01,
		OUTNE_1 = 0x02,
		OUTDRV  = 0x04,
		OCH     = 0x08,
		INVRT   = 0x20,
		// bits 5 - 7 reserved

		MAXIMUM_BITS = 0x08
	}MODE2_BITS;

	PCA9685();
	PCA9685(char address);
	~PCA9685();

	void init(int file, I2C i2cDevice);
	int  readRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, int numberOfRegistersToRead);
	void writeRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, char* dataToWrite, int numberOfRegistersToWrite);
	void setRegister(int i2cFile, char device, char registerNumber);

	void setSleepMode(bool enabled);
	//bool getExternalClockMode(void);

	void setExternalClockMode(bool enabled);
	bool getExternalClockMode(void);

	void setAutoIncrementMode(bool enabled);
	bool getAutoIncrementMode(void);

	void enableSubaddress(char subaddressNumber, bool onOff);
	void enableAllcallAddress(bool onOff);

	void setSubaddress(char subaddressNumber, char subaddress);
	char getSubaddress(char subaddressNumber);

	void setAllcallAddress(char allcallAddress);
	char getAllcallAddress(void);

	void setOutputInverted(bool inverted);
	char getOutputInverted(void);

	void setOutputChange(bool change);
	char getOutputChange(void);

	void setOutputDrive(bool drive);
	char getOutputDrive(void);

	void setOutputNotEnabledMode(char mode);
	char getOutputNotEnabledMode(void);

	void setOnTime(char channel, int onTime);
	int  getOnTime(char channel);

	void setOffTime(char channel, int offTime);
	int  getOffTime(char channel);

	void setPrescaler(char updateRate);
	char getPrescaler(void);

	void setAngle(char output, char angle);

private:

};

