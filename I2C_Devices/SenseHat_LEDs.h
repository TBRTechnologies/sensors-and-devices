/*!
******************************************************************************
* @file    SenseHat_LEDs.h
* @brief   Sensor Implimentation
* @author  Tom Stokland
* @date    3/29/2018
******************************************************************************/
#pragma once
#include "Includes.h"

class SenseHat_LEDs
{
public:
	char deviceAddress = 0x46;	// for LED2472G
	int  i2cFile = 0;
	I2C  i2cDevice;

#define ROWS	8
#define COLUMNS 8
#define BYTES_PER_PIXEL 3
#define COLOR_MASK 0x1F

	enum colors
	{
		COLOR_RED    = 0X001F0000,
		COLOR_ORANGE = 0X001F0F00,
		COLOR_YELLOW = 0X001F1F00,
		COLOR_GREEN  = 0X00001F00,
		COLOR_BLUE   = 0X0000001F,
		COLOR_INDIGO = 0X0007001F,
		COLOR_VIOLET = 0X001F001F,
		COLOR_WHITE  = 0X001F1F1F,
		COLOR_BLACK  = 0X00000000,

		LAST_COLOR	// must be last enum
	};

	char  pixelArray[ROWS][COLUMNS * BYTES_PER_PIXEL];     // 8 rows x 8 columns x 3 bytes (1 each for r,g,b)
	char  dataToWrite[sizeof(pixelArray) + 1];
	int   ledArrayStartRegister = 0;

	SenseHat_LEDs();
	~SenseHat_LEDs();
	void init(int fileNumber, I2C i2cDeviceNumber);

	void generateColorLookupTable(void);
	void convertColorToByteArray(int color, char colorArray[3]);
	char setLEDS(char x, char y, int color, bool immediate);
	void writePixelArray();
	void fill(int color);
	void resetArray(int color);

private:
	void setRegister(int i2cFile, char device, char registerNumber);
	void writeRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, char* dataToWrite, int numberOfRegistersToWrite);

	char checkPixelRange(char x, char y);
};

