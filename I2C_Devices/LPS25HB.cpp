/*!
******************************************************************************
* @file    LPS25HB.cpp
* @brief   Sensor Implimentation
* @author  Tom Stokland
* @date    3/29/2018
******************************************************************************/
#include "Includes.h"

LPS25HB::LPS25HB()
{
	I2C deviceI2C(deviceAddress);
}


LPS25HB::~LPS25HB()
{
}

void LPS25HB::init(int fileNumber, I2C i2cDeviceNumber)
{
	file = fileNumber;
	i2cDevice = i2cDeviceNumber;
	i2cDevice.deviceAddress = deviceAddress;
}

int LPS25HB::readRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, int numberOfRegistersToRead)
{
	if (numberOfRegistersToRead > 1)
	{
		registerNumber |= 0x80; // set bit 7 to read sequential registers
	}
	setRegister(i2cFile, device, registerNumber);
	deviceI2c.readData(registerNumber, dataRead, numberOfRegistersToRead);

	return dataRead[0];
}

//void I2C::writeData(int fileLocation, char startingAddress, char* dataToWrite, int numberOfBytes)
void LPS25HB::writeRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, char* dataToWrite, int numberOfRegistersToWrite)
{
	if (numberOfRegistersToWrite > 1)
	{
		registerNumber |= 0x80; // set bit 7 to read sequential registers
	}
	setRegister(i2cFile, device, registerNumber);
	deviceI2c.writeData(file, registerNumber, dataToWrite, numberOfRegistersToWrite);
}

void LPS25HB::setRegister(int i2cFile, char device, char registerNumber)
{
	if (ioctl(i2cFile, I2C_SLAVE, device) < 0)
	{
		printf("Failed to acquire bus access and/or talk to slave.\n");
		/* ERROR HANDLING; you can check errno to see what went wrong */
		//exit(1);
	}
	dataToWrite[0] = registerNumber;
	if (write(file, dataToWrite, 1) != 1)	// set the starting register
	{
		printf("Failed to set the register address");
	}
}

void LPS25HB::readAll(void)
{
	readRegister(file, i2cDevice, deviceAddress, FIRST_REGISTER, NUMBER_OF_REGISTERS);
}

void LPS25HB::setReferencePressure(int referencePressure)
{
	referencePressure = decimalToTwosComplement(referencePressure, 24);
	dataToWrite[0] = (char)(referencePressure & 0xFF);
	dataToWrite[1] = (char)((referencePressure >> 8) & 0xFF);
	dataToWrite[2] = (char)((referencePressure >> 16) & 0xFF);
	writeRegister(file, i2cDevice, deviceAddress, REF_P_XL, dataToWrite, 3);
}

int  LPS25HB::getReferencePressure(void)
{
	int referencePressure = 0;
	int rawReferencePressure = 0;

	readRegister(file, i2cDevice, deviceAddress, PRESS_OUT_XL, 3);
	rawReferencePressure = (dataRead[2] << 16) + (dataRead[1] << 8) + dataRead[0];
	referencePressure = twosComplementToDecimal(rawReferencePressure, 24);
	return referencePressure;
}

char LPS25HB::whoAmI(void)
{
	return (char)readRegister(file, i2cDevice, deviceAddress, WHO_AM_I, 1);
}

void LPS25HB::setResolution(char resolution)
{
	dataToWrite[0] = resolution;
	writeRegister(file, i2cDevice, deviceAddress, RES_CONF, dataToWrite, 1);
}

char LPS25HB::getResolution(void)
{
	return (char)readRegister(file, i2cDevice, deviceAddress, RES_CONF, 1);
}

void LPS25HB::setControlRegister(char controlRegister, char registerValue)
{
	switch (controlRegister)
	{
	case 1:
		dataToWrite[0] = registerValue;
		writeRegister(file, i2cDevice, deviceAddress, CTRL_REG1, dataToWrite, 1);
		break;

	case 2:
		dataToWrite[0] = registerValue;
		writeRegister(file, i2cDevice, deviceAddress, CTRL_REG2, dataToWrite, 1);
		break;

	case 3:
		dataToWrite[0] = registerValue;
		writeRegister(file, i2cDevice, deviceAddress, CTRL_REG3, dataToWrite, 1);
		break;

	case 4:
		dataToWrite[0] = registerValue;
		writeRegister(file, i2cDevice, deviceAddress, CTRL_REG4, dataToWrite, 1);
		break;

	default:break;

	}
}

char LPS25HB::getControlRegister(char controlRegister)
{
	return (char)readRegister(file, i2cDevice, deviceAddress, (CTRL_REG1 + (controlRegister - 1)), 1);
}

void LPS25HB::setInterruptConfiguration(char interruptConfiguration)
{
	dataToWrite[0] = interruptConfiguration;
	writeRegister(file, i2cDevice, deviceAddress, INTERRUPT_CFG, dataToWrite, 1);
}

char LPS25HB::getInterruptConfiguration(void)
{
	return (char)readRegister(file, i2cDevice, deviceAddress, INTERRUPT_CFG, 1);
}

char LPS25HB::getInterruptSource(void)
{
	return (char)readRegister(file, i2cDevice, deviceAddress, INT_SOURCE, 1);
}

char LPS25HB::getStatus(void)
{
	return (char)readRegister(file, i2cDevice, deviceAddress, STATUS_REG, 1);
}

int  LPS25HB::getPressure(void)
{
	int pressure = 0;
	int rawPressure = 0;

	readRegister(file, i2cDevice, deviceAddress, PRESS_OUT_XL, 3);
	rawPressure = (dataRead[2] << 16) + (dataRead[1] << 8) + dataRead[0];
	pressure = twosComplementToDecimal(rawPressure, 24);
	return pressure;
}

int  LPS25HB::getTemperature(void)
{
	int temperature = 0;
	int rawTemperature = 0;

	rawTemperature = readRegister(file, i2cDevice, deviceAddress, REF_P_XL, 3);
	temperature = twosComplementToDecimal(rawTemperature, 16);
	return temperature;
}

void LPS25HB::setFifoControl(char control)
{
	dataToWrite[0] = control;
	writeRegister(file, i2cDevice, deviceAddress, FIFO_CTRL, dataToWrite, 1);
}

char LPS25HB::getFifoControl(void)
{
	return (char)readRegister(file, i2cDevice, deviceAddress, FIFO_CTRL, 1);
}

char LPS25HB::getFifoStatus(void)
{
	return (char)readRegister(file, i2cDevice, deviceAddress, FIFO_STATUS, 1);
}

void LPS25HB::setPressureThreshold(int pressureThreshold)
{
	pressureThreshold = decimalToTwosComplement(pressureThreshold, 16);
	dataToWrite[0] = (char)(pressureThreshold & 0xFF);
	dataToWrite[1] = (char)((pressureThreshold >> 8) & 0xFF);
	writeRegister(file, i2cDevice, deviceAddress, THS_P_L, dataToWrite, 2);
}

int  LPS25HB::getPressureThreshold(void)
{
	int pressureThreshold = 0;
	int rawPressureThreshold = 0;

	rawPressureThreshold = readRegister(file, i2cDevice, deviceAddress, THS_P_L, 2);
	pressureThreshold = twosComplementToDecimal(rawPressureThreshold, 16);
	return pressureThreshold;
}

void LPS25HB::setPressureOffset(int pressureOffset)
{
	pressureOffset = decimalToTwosComplement(pressureOffset, 16);
	dataToWrite[0] = (char)(pressureOffset & 0xFF);
	dataToWrite[1] = (char)((pressureOffset >> 8) & 0xFF);
	writeRegister(file, i2cDevice, deviceAddress, THS_P_L, dataToWrite, 2);
}

int  LPS25HB::getPressureOffset(void)
{
	int pressureOffset = 0;
	int rawPressureOffset = 0;

	rawPressureOffset = readRegister(file, i2cDevice, deviceAddress, THS_P_L, 2);
	pressureOffset = twosComplementToDecimal(rawPressureOffset, 16);
	return pressureOffset;
}

int LPS25HB::twosComplementToDecimal(int value, int numberOfBits)
{
	if ((value & (1 << numberOfBits)) == 1)
	{
		value = ~value + 1;
	}

	return value;
}

int LPS25HB::decimalToTwosComplement(int value, int numberOfBits)
{
	int mask = 0xFFFF;

	if (value < 0)
	{
		mask >>= (32 - numberOfBits);
		value = (mask ^ value) - 1;
	}
	return value;
}

