#include "Includes.h"

SenseHat_JoyStick::SenseHat_JoyStick()
{
}


SenseHat_JoyStick::~SenseHat_JoyStick()
{
}

void SenseHat_JoyStick::init(int fileNumber, I2C i2cDeviceNumber)
{
	file = fileNumber;
	i2cDevice = i2cDeviceNumber;
}

int SenseHat_JoyStick::readRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, int numberOfRegistersToRead)
{
	setRegister(i2cFile, device, registerNumber);
	dataRead[0] = 0xF2;
	deviceI2c.readData(registerNumber, dataRead, 1);

	return dataRead[0];
}

void SenseHat_JoyStick::setRegister(int i2cFile, char device, char registerNumber)
{
	if (ioctl(i2cFile, I2C_SLAVE, device) < 0)
	{
		printf("Failed to acquire bus access and/or talk to slave.\n");
		/* ERROR HANDLING; you can check errno to see what went wrong */
		//exit(1);
	}
	dataToWrite[0] = registerNumber;
	if (write(file, dataToWrite, 1) != 1)	// set the starting register
	{
		printf("Failed to set the register address");
	}
}

char SenseHat_JoyStick::readJoystick(void)
{
	return readRegister(file, i2cDevice, deviceAddress, commandId, 1);
}

bool SenseHat_JoyStick::getHoldStatus(void)
{
	return holding;
}

char SenseHat_JoyStick::ISR_Joystick(void)
{
	char joystickValue = 0;
	//if (interruptPin == 0)
	//{
	//	holding = false;
	//}
	//else
	//{
	//	holding = true;
	//}
	joystickValue = readRegister(file, i2cDevice, deviceAddress, commandId, 1);
	return joystickValue;
}
