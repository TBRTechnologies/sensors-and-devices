#pragma once
#include "Includes.h"

class LSM9DS1
{
public:
	char deviceAddressAccelGyro = 0x6a;
	char deviceAddressMagnetometer = 0x1c;
	int  file = 0;
	I2C  i2cDevice;
	char dataRead[0x3B];
	char dataToWrite[3];

	LSM9DS1();
	~LSM9DS1();

	void init(int fileNumber, I2C i2cDeviceNumber);
	int readRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, int numberOfRegistersToRead);
	void writeRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, char* dataToWrite, int numberOfRegistersToWrite);
	void setRegister(int i2cFile, char device, char registerNumber);

#pragma region accelerometer/gyro

//  * ACT_THS r / w 04 00000100 00000000
//	* ACT_DUR r / w 05 00000101 00000000
//	* INT_GEN_CFG_XL r / w 06 00000110 00000000
//	* INT_GEN_THS_X_XL r / w 07 00000111 00000000
//	* INT_GEN_THS_Y_XL r / w 08 00001000 00000000
//	* INT_GEN_THS_Z_XL r / w 09 00001001 00000000
//	* INT_GEN_DUR_XL r / w 0A 00001010 00000000
//	* REFERENCE_G r / w 0B 00001011 00000000
//	* INT1_CTRL r / w 0C 00001100 00000000
//	* INT2_CTRL r / w 0D 00001101 00000000
//	* Reserved-- 0E-- --Reserved
//	* WHO_AM_I r 0F 00001111 01101000
//	* CTRL_REG1_G r / w 10 00010000 00000000
//	* CTRL_REG2_G r / w 11 00010001 00000000
//	* CTRL_REG3_G r / w 12 00010010 00000000
//	* ORIENT_CFG_G r / w 13 00010011 00000000
//	* INT_GEN_SRC_G r 14 00010100 output
//	* OUT_TEMP_L r 15 00010101 output
//	* OUT_TEMP_H r 16 00010110 output
//	* STATUS_REG r 17 00010111 output
//	* OUT_X_L_G r 18 00011000 output
//	* OUT_X_H_G r 19 00011001 output
//	* OUT_Y_L_G r 1A 00011010 output
//	* OUT_Y_H_G r 1B 00011011 output
//	* OUT_Z_L_G r 1C 00011100 output
//	* OUT_Z_H_G r 1D 00011101 output
//	* CTRL_REG4 r / w 1E 00011110 00111000
//	* CTRL_REG5_XL r / w 1F 00011111 00111000DocID025715 Rev 3 39 / 72
//	* CTRL_REG6_XL r / w 20 00100000 00000000
//	* CTRL_REG7_XL r / w 21 00100001 00000000
//	* CTRL_REG8 r / w 22 00100010 00000100
//	* CTRL_REG9 r / w 23 00100011 00000000
//	* CTRL_REG10 r / w 24 00100100 00000000
//	* Reserved-- 25 -- --Reserved
//	* INT_GEN_SRC_XL r 26 00100110 output
//	* STATUS_REG r 27 00100111 output
//	* OUT_X_L_XL r 28 00101000 output
//	* OUT_X_H_XL r 29 00101001 output
//	* OUT_Y_L_XL r 2A 00101010 output
//	* OUT_Y_H_XL r 2B 00101011 output
//	* OUT_Z_L_XL r 2C 00101100 output
//	* OUT_Z_H_XL r 2D 00101101 output
//	* FIFO_CTRL r / w 2E 00101110 00000000
//	* FIFO_SRC r 2F 00101111 output
//	* INT_GEN_CFG_G r / w 30 00110000 00000000
//	* INT_GEN_THS_XH_G r / w 31 00110001 00000000
//	* INT_GEN_THS_XL_G r / w 32 00110010 00000000
//	* INT_GEN_THS_YH_G r / w 33 00110011 00000000
//	* INT_GEN_THS_YL_G r / w 34 00110100 00000000
//	* INT_GEN_THS_ZH_G r / w 35 00110101 00000000
//	* INT_GEN_THS_ZL_G r / w 36 00110110 00000000
//	* INT_GEN_DUR_G r / w 37 00110111 00000000

	enum accel_gyro_registers
	{
		START_OF_REGISTERS = 0,
		ACT_THS            = 0x04, // r/w
		ACT_DUR            = 0x05, // r/w
		INT_GEN_CFG_XL     = 0x06, // r/w
		INT_GEN_THS_X_XL   = 0x07, // r/w
		INT_GEN_THS_Y_XL   = 0x08, // r/w
		INT_GEN_THS_Z_XL   = 0x09, // r/w
		INT_GEN_DUR_XL     = 0x0A, // r/w
		REFERENCE_G        = 0x0B, // r/w
		INT1_CTRL          = 0x0C, // r/w
		INT2_CTRL          = 0x0D, // r/w
		RESERVED_0x0E_XL   = 0x0E,
		WHO_AM_I           = 0x0F, // r
		CTRL_REG1_G        = 0x10, // r/w
		CTRL_REG2_G        = 0x11, // r/w
		CTRL_REG3_G        = 0x12, // r/w
		CTRL_REG4          = 0x1E, // r/w
		CTRL_REG5_XL       = 0x1F, // r/w
		CTRL_REG6_XL       = 0x20, // r/w
		CTRL_REG7_XL       = 0x21, // r/w
		CTRL_REG8          = 0x22, // r/w
		CTRL_REG9          = 0x23, // r/w
		CTRL_REG10         = 0x24, // r/w
		RESERVED_0x25      = 0x25,
		INT_GEN_SRC_XL     = 0x26, // r
		ORIENT_CFG_G       = 0x13, // r/w
		INT_GEN_SRC_G      = 0x14, // r
		OUT_TEMP_L         = 0x15, // r
		OUT_TEMP_H         = 0x16, // r
		STATUS_REG         = 0x17, // r (also 0x27 is the same)
		OUT_X_L_G          = 0x18, // r
		OUT_X_H_G          = 0x19, // r
		OUT_Y_L_G          = 0x1A, // r
		OUT_Y_H_G          = 0x1B, // r
		OUT_Z_L_G          = 0x1C, // r
		OUT_Z_H_G          = 0x1D, // r
		OUT_X_L_XL         = 0x28, // r
		OUT_X_H_XL         = 0x29, // r
		OUT_Y_L_XL         = 0x2A, // r
		OUT_Y_H_XL         = 0x2B, // r
		OUT_Z_L_XL         = 0x2C, // r
		OUT_Z_H_XL         = 0x2D, // r    
		FIFO_CTRL          = 0x2E, // r/w
		FIFO_SRC           = 0x2f, // r
		INT_GEN_CFG_G      = 0x30, // r/w
		INT_GEN_THS_XH_G   = 0x31, // r/w
		INT_GEN_THS_XL_G   = 0x32, // r/w
		INT_GEN_THS_YH_G   = 0x33, // r/w
		INT_GEN_THS_YL_G   = 0x34, // r/w
		INT_GEN_THS_ZH_G   = 0x35, // r/w
		INT_GEN_THS_ZL_G   = 0x36, // r/w
		INT_GEN_DUR_G      = 0x37, // r/w
		END_OF_REGISTERS,	// keep as last entry
	};

	void setActivityThreshold(char threshold);
	char getActivityThreshold(void);

	void setInactivityDuration(char duration);
	char getInactivityDuration(void);

	void setInterruptGenerationXl(char configuration);
	char getInterruptGenerationXl(void);

	void setInterruptThreshold(char axis, char threshold);
	char getInterruptThreshold(char axis);

	void setReference(char reference);
	char getReference(void);

	void setInterrupt_1_Control(char configuration);
	char getInterrupt_1_Control(void);

	void setInterrupt_2_Control(char configuration);
	char getInterrupt_2_Control(void);

	char whoAmI(void);

	void setControlRegisterAG(char registerToWrite, char value);
	char getControlRegisterAG(char registerToRead);

	void setOrientation(char orientation);
	char getOrientation(void);

	char getInterruptSourceAG(void);

	int  getTemperature(void);

	int  getGyroscope(char axis);

	char getInterruptSourceXl(void);

	char getStatusAG(void);

	int  getAccelerometer(char axis);

	void setFifoControl(char control);
	char getFifoControl(void);
	char getFifoSource(void);

	void setInterruptConfigurationGyro(char configuration);
	char getInterruptConfigurationGyro(void);

	void setInterruptGeneratorThresholdGyro(char axis, int threshold);
	int  getInterruptGeneratorThresholdGyro(char axis);

	void setInterruptGeneratorDurationGyro(char duration);
	char getInterruptGeneratorDurationGyro(void);

#pragma endregion

#pragma region magnetometer

		//	Reserved r 38 - 7F-- --Reserved
//	* Reserved 00 - 04 -- --Reserved
//	* OFFSET_X_REG_L_M r / w 05 00000000
//	Offset in order to compensate
//	environmental effects
//	* OFFSET_X_REG_H_M r / w 06 00000000
//	* OFFSET_Y_REG_L_M r / w 07 00000000
//	* OFFSET_Y_REG_H_M r / w 08 00000000
//	* OFFSET_Z_REG_L_M r / w 09 00000000
//	* OFFSET_Z_REG_H_M r / w 0A 00000000
//	* Reserved 0B - 0E-- --Reserved
//	* WHO_AM_I_M r 0F 0000 1111 00111101 Magnetic Who I am ID
//	* Reserved 10 - 1F-- --Reserved
//	* CTRL_REG1_M r / w 20 0010 0000 00010000
//	* CTRL_REG2_M r / w 21 0010 0001 00000000
//	* CTRL_REG3_M r / w 22 0010 0010 00000011
//	* CTRL_REG4_M r / w 23 0010 0011 00000000
//	* CTRL_REG5_M r / w 24 0010 0100 00000000
//	* Reserved 25 - 26 -- --Reserved
//	* STATUS_REG_M r 27 0010 0111 Output
//	* OUT_X_L_M r 28 0010 1000 Output
//	* OUT_X_H_M r 29 0010 1001 Output
//	* OUT_Y_L_M r 2A 0010 1010 Output
//	* OUT_Y_H_M r 2B 0010 1011 Output
//	* OUT_Z_L_M r 2C 0010 1100 Output
//	* OUT_Z_H_M r 2D 0010 1101 Output
//	* Reserved r 2E-2F -- --Reserved
//	* INT_CFG_M rw 30 00110000 00001000 Magnetic interrupt configuration
//	register
//	* INT_SRC_M r 31 00110001 00000000 Magnetic interrupt generator
//	status register
//	* INT_THS_L_M r 32 00110010 00000000 Magnetic interrupt generator
//	* INT_THS_H_M r 33 00110011 00000000 threshold

enum magnetic_registers
	{
		START_OF_REGISTERS_M = 0,
		RESERVED_0x00 = 0x00,
		RESERVED_0x01 = 0x01,
		RESERVED_0x02 = 0x02,
		RESERVED_0x03 = 0x03,
		RESERVED_0x04 = 0x04,
		OFFSET_X_REG_L_M = 0x05,	// r/w
		OFFSET_X_REG_H_M = 0x06,	// r/w
		OFFSET_Y_REG_L_M = 0x07,	// r/w
		OFFSET_Y_REG_H_M = 0x08,	// r/w
		OFFSET_Z_REG_L_M = 0x09,	// r/w
		OFFSET_Z_REG_H_M = 0x0A,	// r/w
		RESERVED_0x0B = 0x0B,
		RESERVED_0x0C = 0x0C,
		RESERVED_0x0D = 0x0D,
		RESERVED_0x0E = 0x0E,
		WHO_AM_I_M = 0x0F,	// r
		RESERVED_0x10_0x1F = 0x1F,	// all from 0x10 to 0x1F
		CTRL_REG1_M = 0x20,	//  r/w
		CTRL_REG2_M = 0x21,	//  r/w
		CTRL_REG3_M = 0x22,	//  r/w
		CTRL_REG4_M = 0x23,	//  r/w
		CTRL_REG5_M = 0x24,	//  r/w
		RESERVED_0x25_M = 0x25,
		RESERVED_0x26 = 0x26,
		STATUS_REG_M = 0x27,	// r
		OUT_X_L_M = 0x28,	// r
		OUT_X_H_M = 0x29,	// r
		OUT_Y_L_M = 0x2A,	// r
		OUT_Y_H_M = 0x2B,	// r
		OUT_Z_L_M = 0x2C,	// r
		OUT_Z_H_M = 0x2D,	// r
		RESERVED_0x2E_0x2F = 0x2F,
		INT_CFG_M = 0x30,	// r/w
		INT_SRC_M = 0x31,	// r
		INT_THS_L_M = 0x32,	// r
		INT_THS_H_M = 0x33,	// r
		END_OF_REGISTERS_M,	// keep as last entry
	};

	void setOffsetM(char axis, int offset);
	int  getOffsetM(char axis);

	char getWhoAmIM(void);

	void setControlRegisterM(char controlRegister, int value);
	char getControlRegisterM(char controlRegister);

	char getStatusRegisterM(void);

	int  getMagnetometer(char axis);

	void setInterruptConfiguration(char configuration);
	char getInterruptConfiguration(void);

	char getInterruptSourceM(void);

	void  setInterruptThresholdRegister(int threshold);
	int   getInterruptThresholdRegister(void);

#pragma endregion

private:
	int  speed = 100; // 100k

	int  readRegister(char registerNumber);
	void writeRegister(char registerNumber, char value);

	inline int twosComplementToDecimal(int value, int numberOfBits)
	{
		if ((value & (1 << numberOfBits)) == 1)
		{
			value = ~value + 1;
		}

		return value;
	}

	inline int decimalToTwosComplement(int value, int numberOfBits)
	{
		int mask = 0xFFFF;

		if (value < 0)
		{
			mask >>= (32 - numberOfBits);
			value = (mask ^ value) - 1;
		}
		return value;
	}
};

