/*!
******************************************************************************
* @file    SenseHat_LEDs.cpp
* @brief   Sensor Implimentation
* @author  Tom Stokland
* @date    3/29/2018
******************************************************************************/
#include "Includes.h"

SenseHat_LEDs::SenseHat_LEDs()
{
}

SenseHat_LEDs::~SenseHat_LEDs()
{
}

void SenseHat_LEDs::init(int fileNumber, I2C i2cDeviceNumber)
{
	i2cFile = fileNumber;
	i2cDevice = i2cDeviceNumber;
	char j, k;

	// fill array with 0's (= BLACK)
	resetArray(COLOR_BLACK);

	for (j = 1; j < sizeof(dataToWrite) + 1; j++)
	{
		//if ((j % 3) == 2)
		{
			dataToWrite[j] = 0x3f;// &COLOR_MASK | 0x00;
		}
	}
	//dataToWrite[0] = 0x00;
	//dataToWrite[1] = 0x1f;
	//dataToWrite[9] = 0x0f;
	//dataToWrite[17] = 0x00;
	// write the array to the driver
//	setRegister(i2cFile, deviceAddress, ledArrayStartRegister);
	i2cDevice.writeData(i2cFile, ledArrayStartRegister, dataToWrite, sizeof(dataToWrite));
//	i2cDevice.writeData(i2cFile, 0, fake, 3);

	fill(COLOR_INDIGO);
//	setRegister(i2cFile, deviceAddress, ledArrayStartRegister);
	i2cDevice.writeData(i2cFile, ledArrayStartRegister, dataToWrite, sizeof(dataToWrite));

	setLEDS(0, 0, COLOR_RED, true);
	setLEDS(1, 1, COLOR_ORANGE, true);
	setLEDS(2, 2, COLOR_YELLOW, true);
	setLEDS(3, 3, COLOR_GREEN, true);
	setLEDS(4, 4, COLOR_BLUE, true);
	setLEDS(5, 5, COLOR_BLACK, true);
	setLEDS(6, 6, COLOR_VIOLET, true);
	setLEDS(7, 7, COLOR_WHITE, true);
}

void SenseHat_LEDs::writeRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, char* dataToWrite, int numberOfRegistersToWrite)
{
	if (numberOfRegistersToWrite > 1)
	{
		registerNumber |= 0x80; // set bit 7 to read sequential registers
	}
//  	setRegister(i2cFile, device, registerNumber);
	deviceI2c.writeData(i2cFile, registerNumber, dataToWrite, numberOfRegistersToWrite);
}

//void SenseHat_LEDs::setRegister(int i2cFile, char device, char registerNumber)
//{
//	if (ioctl(i2cFile, I2C_SLAVE, device) < 0)
//	{
//		printf("Failed to acquire bus access and/or talk to slave.\n");
//		/* ERROR HANDLING; you can check errno to see what went wrong */
//		//exit(1);
//	}
//	dataToWrite[0] = registerNumber;
//	if (write(i2cFile, dataToWrite, 1) != 1)	// set the starting register
//	{
//		printf("Failed to set the register address");
//	}
//}

void SenseHat_LEDs::generateColorLookupTable(void)
{

}

void SenseHat_LEDs::convertColorToByteArray(int color, char colorArray[])
{
	colorArray[0] = (char)(color >> 16) & COLOR_MASK;
	colorArray[1] = (char)(color >> 8) & COLOR_MASK;
	colorArray[2] = (char)color & COLOR_MASK;
}

char SenseHat_LEDs::setLEDS(char x, char y, int color, bool immediate)

{
	char colorArray[3];
	dataToWrite[0] = 0;

	if (checkPixelRange(x, y) != 0)
	{
		return -1;
	}
	else
	{
		convertColorToByteArray(color, colorArray);
		char redAddress = y * (COLUMNS * BYTES_PER_PIXEL) + x + 1;
		dataToWrite[redAddress] = colorArray[0];
		dataToWrite[redAddress + 8] = colorArray[1];
		dataToWrite[redAddress + 16] = colorArray[2];

		if (immediate)
		{
			i2cDevice.writeData(i2cFile, ledArrayStartRegister, dataToWrite, sizeof(dataToWrite));
		}
	}
}

void SenseHat_LEDs::writePixelArray()
{
	;
}

char SenseHat_LEDs::checkPixelRange(char x, char y)
{
	if ((x < 0) || (x > COLUMNS))
	{
		return -1;
	}
	else if ((y < 0) || (y > ROWS))
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

void SenseHat_LEDs::fill(int color)
{
	char colorData[3];
	int index0, index1;

	dataToWrite[0] = 0x00;

	convertColorToByteArray(color, colorData);

	for (index0 = 0; index0 < (ROWS * COLUMNS * BYTES_PER_PIXEL); index0 += (COLUMNS * BYTES_PER_PIXEL))
	{
		for (index1 = 0; index1 < COLUMNS; index1++)
		{
			dataToWrite[(index0) + (index1 + 1)] = colorData[0];
			dataToWrite[(index0) + (index1 + 1) + 8] = colorData[1];
			dataToWrite[(index0) + (index1 + 1) + 16] = colorData[2];
		}
	}
}

void SenseHat_LEDs::resetArray(int color)
{
	fill(color);
	i2cDevice.writeData(i2cFile, ledArrayStartRegister, dataToWrite, sizeof(dataToWrite));


}


