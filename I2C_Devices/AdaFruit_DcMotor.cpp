#include "Includes.h"

//AdaFruit_DcMotor::AdaFruit_DcMotor(char address)
//{
//	deviceAddress = address;
//}

AdaFruit_DcMotor::AdaFruit_DcMotor()
{
}

AdaFruit_DcMotor::~AdaFruit_DcMotor()
{
}

void AdaFruit_DcMotor::init(int fileNumber, I2C i2cDeviceNumber)
{
	file = fileNumber;
	i2cDevice = i2cDeviceNumber;
	i2cDevice.deviceAddress = deviceAddress;
}

int  AdaFruit_DcMotor::readRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, int numberOfRegistersToRead)
{
	if (numberOfRegistersToRead > 1)
	{
		registerNumber |= 0x80; // set bit 7 to read sequential registers
	}
	setRegister(i2cFile, device, registerNumber);
	deviceI2c.readData(registerNumber, dataRead, numberOfRegistersToRead);

	return dataRead[0];
}

void AdaFruit_DcMotor::writeRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, char* dataToWrite, int numberOfRegistersToWrite)
{
	// for PCA9685 check MODE_1.5 -> if 1 auto increment, else write numberOfBytes times
	//if (numberOfRegistersToWrite > 1)
	//{
	//	registerNumber |= 0x80; // set bit 7 to read sequential registers
	//}
	setRegister(i2cFile, device, registerNumber);
	deviceI2c.writeData(file, registerNumber, dataToWrite, numberOfRegistersToWrite);
}

void AdaFruit_DcMotor::setRegister(int i2cFile, char device, char registerNumber)
{
	if (ioctl(i2cFile, I2C_SLAVE, device) < 0)
	{
		printf("Failed to acquire bus access and/or talk to slave.\n");
		/* ERROR HANDLING; you can check errno to see what went wrong */
		//exit(1);
	}
	dataToWrite[0] = registerNumber;
	if (write(file, dataToWrite, 1) != 1)	// set the starting register
	{
		printf("Failed to set the register address");
	}
}


