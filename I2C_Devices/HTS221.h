#pragma once
#include "Includes.h"


class HTS221
{
public:
	char deviceAddress = 0x5f;
	int  file = 0;
	I2C  i2cDevice;
	char dataRead[0x3B];
	char dataToWrite[3];

	HTS221();
	~HTS221();

	enum registers
	{
		FIRST_REGISTER =      0x00,
		WHO_AM_I =            0x0F,
		AV_CONF =             0x10,
		CTRL_REG1 =           0x20,
		CTRL_REG2 =           0x21,
		CTRL_REG3 =           0x22,
		STATUS_REG =          0x27,
		HUMIDITY_OUT_L =      0x28,
		HUMIDITY_OUT_H =      0x29,
		TEMP_OUT_L =          0x2A,
		TEMP_OUT_H =          0x2B,
		CALIB_0 =             0x30,	// 0x30 - 0x3F, do not modify
		NUMBER_OF_REGISTERS = 0x40,	// must be last
	};

	void init(int fileNumber, I2C i2cDeviceNumber);
	int  readRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, int numberOfRegistersToRead);
	void writeRegister(int i2cFile, I2C deviceI2c, char device, char registerNumber, char* dataToWrite, int numberOfRegistersToWrite);
	void setRegister(int i2cFile, char device, char registerNumber);

	void readAll(void);

	char whoAmI(void);

	int setResolution(bool temperatureResolution, int resolution);
	int  getResolution(void);

	int  setControlRegister(char controlRegister, char registerValue);
	char getControlRegister(char controlRegister);

	char getStatus(void);

	int getHumidity();
	int getTemperature();

private:
	int twosComplementToDecimal(int value, int numberOfBits);
	int decimalToTwosComplement(int value, int numberOfBits);
};

