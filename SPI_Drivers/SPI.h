#pragma once

class SPI
{
public:
	int commSpeed = 1000; // 1M
	char spiBuffer[30];
	int file;
	char* filename;
	const char *buffer;

	SPI();
	SPI(char deviceAddress);
	SPI(char deviceAddress, int speed);

	~SPI();

	int init(char deviceAddress);
	void setRegister(char register);
	void writeData(int fileLocation, char startingAddress, char* dataToWrite, int numberOfBytes);
	void readData(char startingAddress, char* dataRead, int numberOfBytes);
};

